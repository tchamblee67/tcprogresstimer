A SpriteKit progress timer, roughly equivalent to CCProgressTimer.

![TCProgressTimerRevisted.gif](https://bitbucket.org/repo/KX6X9L/images/1626289945-TCProgressTimerRevisted.gif)

http://tonychamblee.com/2013/11/18/tcprogresstimer-a-spritekit-progress-timer/