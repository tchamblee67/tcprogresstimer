//
//  TCMyScene.m
//  TCProgressTimerDemo
//
//  Created by Tony Chamblee on 11/17/13.
//  Copyright (c) 2013 Tony Chamblee. All rights reserved.
//

#import "TCMyScene.h"
#import "TCProgressTimerNode.h"

#define kCyclesPerSecond 0.25f

@interface TCMyScene ()

@property (nonatomic, strong) TCProgressTimerNode *progressTimerNode1;
@property (nonatomic, strong) TCProgressTimerNode *progressTimerNode2;
@property (nonatomic, strong) TCProgressTimerNode *progressTimerNode3;
@property (nonatomic, strong) TCProgressTimerNode *progressTimerNode4;

@property (nonatomic) NSTimeInterval startTime;

@end

@implementation TCMyScene

- (id)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    
    if (self)
    {
        self.backgroundColor = [SKColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
        
        _progressTimerNode1 = [[TCProgressTimerNode alloc] initWithForegroundImageNamed:@"progress_foreground"
                                                                  backgroundImageNamed:nil
                                                                   accessoryImageNamed:nil];
        _progressTimerNode1.position = CGPointMake(roundf(size.width / 5.0f), roundf(size.height / 2.0f));
        [self addChild:_progressTimerNode1];
        _progressTimerNode1.progress = 0.5;
        
        _progressTimerNode2 = [[TCProgressTimerNode alloc] initWithForegroundImageNamed:@"progress_foreground"
                                                                   backgroundImageNamed:@"progress_background"
                                                                    accessoryImageNamed:nil];
        _progressTimerNode2.position = CGPointMake(2 * roundf(size.width / 5.0f), roundf(size.height / 2.0f));
        [self addChild:_progressTimerNode2];
        _progressTimerNode2.progress = 0.5;
        
        _progressTimerNode3 = [[TCProgressTimerNode alloc] initWithForegroundImageNamed:@"progress_foreground"
                                                                   backgroundImageNamed:@"progress_background"
                                                                    accessoryImageNamed:@"progress_accessory"];
        _progressTimerNode3.position = CGPointMake(3 * roundf(size.width / 5.0f), roundf(size.height / 2.0f));
        [self addChild:_progressTimerNode3];
        _progressTimerNode3.progress = 0.5;
        
        _progressTimerNode4 = [[TCProgressTimerNode alloc] initWithRadius:14.0
                                                          backgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5]
                                                          foregroundColor:[UIColor colorWithRed:200.0/255.0 green:0.0 blue:0.0 alpha:1.0]];
        _progressTimerNode4.position = CGPointMake(4 * roundf(size.width / 5.0f), roundf(size.height / 2.0f));
        [self addChild:_progressTimerNode4];
        _progressTimerNode4.progress = 0.5;
        
        self.startTime = CACurrentMediaTime();
    }
    
    return self;
}

- (void)update:(NSTimeInterval)currentTime
{
    [super update:currentTime];
    
    CGFloat secondsElapsed = currentTime - self.startTime;
    CGFloat cycle = secondsElapsed * kCyclesPerSecond;
    CGFloat progress = cycle - (NSInteger)cycle;
    
    self.progressTimerNode1.progress = progress;
    self.progressTimerNode2.progress = progress;
    self.progressTimerNode3.progress = progress;
    self.progressTimerNode4.progress = progress;
}

@end
